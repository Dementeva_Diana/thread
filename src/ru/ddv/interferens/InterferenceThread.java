package ru.ddv.interferens;


public class InterferenceThread extends Thread {
    private final InterferenceExample checker;
    private volatile static int i;

    InterferenceThread(String name, InterferenceExample checker) {
        super(name);
        this.checker = checker;
    }

    public void run() {
        System.out.println(this.getName() + " запущен");
        while (!checker.stop()) {
            increment();
        }
        System.out.println(this.getName() + " завершен");
    }

    /**
     * Метод инкрементации переменной i.
     */
    private static synchronized void increment() {
        ++i;
    }

    int getI() {
        return i;
    }

}