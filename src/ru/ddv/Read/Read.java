package ru.ddv.Read;

import java.io.*;

public class Read extends Thread{
    public String read;
    public String write;


    Read (String read, String write) {
        this.read = read;
        this.write = write;
    }

    @Override
    public void run() {

        try (BufferedReader reader = new BufferedReader(new FileReader(read));
             BufferedWriter writer = new BufferedWriter(new FileWriter(write, true))) {
            String line;
            long startThreadTime = System.currentTimeMillis();
            while ((line = reader.readLine()) != null) {
                writer.write(line + "\n");
            }

            System.out.println("Время выполнения потока =  " + (System.currentTimeMillis() - startThreadTime));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
