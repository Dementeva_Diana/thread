package ru.ddv.Catch;
/** Класс для потоков
 *
 */
public class Player1 extends Thread {
    private  String name;
    private int priority2;
    private  int priority1;
    /** конструктор
     *
     * @param name
     * @param priority1
     * @param priority2
     */
    public Player1(String name, int priority1, int priority2) {
        this.priority2 = priority2;
        this.name = name;
        this.priority1 = priority1;
    }
    /**
     * конструктор по умолчанию
     */
    public Player1() {
        this("name",10, 1);
    }
    public void run (){
        setPriority(priority1);
        for ( int i=0; i<100; i++){
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(name);
            if (i==50){
                setPriority(priority2);
            }
        }
    }
}
