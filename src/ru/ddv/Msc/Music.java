package ru.ddv.Msc;

import java.io.*;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Music {

    private static final String IN_FILE_TXT = "src\\ru\\ddv\\Msc\\inFile.txt";
    private static final String OUT_FILE_TXT = "src\\ru\\ddv\\Msc\\outFile.txt";
    private static final String PATH_TO_MUSIC = "src\\ru\\ddv\\Msc\\music\\music";


    public static void main(String[] args) {
        String Url;
        try (BufferedReader inFile = new BufferedReader(new FileReader(IN_FILE_TXT))) {
            String result;
            String link;
            while ((link = inFile.readLine()) != null) {
                URL url = new URL(link);
                result = inp(url);
                seaLink(result, OUT_FILE_TXT);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    public static void mus() {
        try (BufferedReader musicFile = new BufferedReader(new FileReader(OUT_FILE_TXT))) {
            String music;
            try {

                for (int count = 0; (music = musicFile.readLine()) != null; count++) {
                    Download mscthread = new Download(music, PATH_TO_MUSIC + String.valueOf(count) + ".mp3");
                    mscthread.start();
                    mscthread.join();
                }
            } catch (InterruptedException f) {
                f.printStackTrace();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void seaLink(String result, String OUT_FILE_TXT) {
        try (BufferedWriter outFile = new BufferedWriter(new FileWriter(OUT_FILE_TXT))) {
            System.out.println("Ждите, идет поиск ссылок на музон");
            Pattern email_pattern = Pattern.compile("\\s*(?<=data-url\\s?=\\s?\")[^>]*\\/*(?=\")");
            Matcher matcher = email_pattern.matcher(result);
            for (int i = 0; matcher.find() && i < 2; i++) {
                outFile.write(matcher.group() + "\r\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String inp(URL url) {
        String result = null;
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()))) {
            result = bufferedReader.lines().collect(Collectors.joining("\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }
}

