package ru.ddv.Flow;

public class Main {
    public static void main (String [] args){
        Egg egg = new Egg();	//Создание потока
        Chicken chicken = new Chicken();
        System.out.println("Спор начат...");
        egg.start();
        chicken.start();
        if(egg.isAlive())
        {
            try{
                egg.join();
            }catch(InterruptedException e){}

            System.out.println("Первым появилось яйцо!");
        }
        else
        {
            System.out.println("Первой появилась курица!");
        }
        System.out.println("Спор закончен!");
    }
}